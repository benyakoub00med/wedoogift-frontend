# WedoogiftFrontend
[![version][version-badge]][CHANGELOG]

[![version][version-badge]][CHANGELOG]

### Features

- Allow users to find a combination of cards allowing him to reach the desired value.
- Display a list of the cards needed to reach that amount.
- Only integer amounts are possible.

### Technical Features

- You can add more option for the consumption of theAPI
- You can easely transform the project into an NX-project
- Change with ease the API endpoints and configuration
<<<<<<< HEAD
- Adapte the
=======
- Adapte the 
>>>>>>> fb1d7ebe7c889cec21570449d9d71994d8c3302c

## Link

- [Angular documentation][link1]

- [Bootstap doc][link2]

- [Cypress doc][link3]

## Terminal Commands

- Install NodeJs from [Node.js Official Page](https://nodejs.org/) v12+ to run.
- Open Terminal
- Go to your file project `cd <file-project-name>`
- Run in terminal: `npm install -g @angular/cli` to install angular cli globaly.
- Then: `npm install`to install all the required dependencies
- And: `npm start` to run a developpement server localy
- > Navigate to localhost:4200 : so you can see the view

## What's included

Within the download you'll find the following directories and files:

```
├───.vscode
├───cypress
│   ├───fixtures
│   ├───integration
│   ├───plugins
│   ├───screenshots
│   │   └───spec.ts
│   ├───support
│   └───videos
└───src
    ├───app
    │   ├───cards-dispatcher
    │   │   ├───components
    │   │   └───services
    │   │       └───api-calculation
    │   └───shared
    │       ├───components
    │       │   ├───footer
    │       │   ├───forbidden
    │       │   ├───header
    │       │   └───not-found
    │       ├───models
    │       └───services
    │           └───token-injector
    ├───assets
    │   └───images
    └───environments
  ```

# Card Dispatcher

## Screen-shot

![alt text]((<https://firebasestorage.googleapis.com/v0/b/benyakoub.appspot.com/o/Process%20Flow.png?alt=media&token=058d9ca5-954b-47be-9174-0645746ac1b7>? "Screen shot of the running app")

[Click to see the screen shot](https://gitlab.com/benyakoub00med/wedoogift-frontend/-/blob/main/maquette.PNG)

![](https://gitlab.com/benyakoub00med/wedoogift-frontend/-/blob/main/maquette.PNG)

### Get Started

`$ cd wedoogift-frontend`
`$ npm run dev`

> Lunch the API on <http://localhost:3000>

> Lunch the client app <http://localhost:4200>

## Demo-app/code

<https://stackblitz.com/benmed00/wedoogift-frontend>

### Flow Chart

[Flow colored Chart](https://firebasestorage.googleapis.com/v0/b/benyakoub.appspot.com/o/Process%20Flow.png?alt=media&token=058d9ca5-954b-47be-9174-0645746ac1b7)

<https://gitlab.com/benyakoub00med/wedoogift-frontend/-/blob/main/Process_Flow.png>

## Contribution

This is a simple project, under the current licence you can use it & fork-it as you wish,also you have ideas or you want to contribute please feel free to submit a "Pull request" / "Merge request" or send me an e-mail to my adresse : benyakoub.pro@gmail.com

### Licence

MIT

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


[link1]: <https://angular.io/>
[link2]: <https://getbootstrap.com/>
[link3]: <https://www.cypress.io/>


### End
